# PrestaShop-Ansible

[PrestaShop](http://www.prestashop.com/) workspace for development and testing. Is not intended for production.

Based on a Vagrant VirtualBox (Ubuntu 16.04 LTS [xenial64](https://vagrantcloud.com/ubuntu/boxes/xenial64)) provisioned by [Ansible](http://www.ansible.com/) with:

* Apache 2.4
* PHP 7
* MySQL 5.7
* PrestaShop

## Requirements

You need [VirtualBox](http://www.virtualbox.org/), [Vagrant](http://www.vagrantup.com/) and [Ansible](http://www.ansible.com/) installed on your system.

## Usage

**Clone this repository**

```
$ git clone https://gitlab.com/stekan/prestashop-ansible.git
```

**Boot the box**

```
$ cd PrestaShop-Ansible
$ vagrant up
```

**Add the following line into your hosts file (/etc/hosts on Linux/MacOS, other operating systems see [here](http://en.wikipedia.org/wiki/Hosts_(file)#Location_in_the_file_system))**

```
127.0.0.1       prestashop.test
```

### Open PrestaShop in your Browser

* Frontend: `http://prestashop.test:8080`
* Backoffice: `http://prestashop.test:8080/admin/`
  * E-Mail: pub@prestashop.com
  * Password: 0123456789

## Synchronization

To activate synced folder set `disabled: false` in line `config.vm.synced_folder` in file `Vagrantfile`.

Vagrant synced folder type:
- [rsync](https://www.vagrantup.com/docs/synced-folders/rsync.html)

Vagrant Plugins:
- [vagrant-rsync-back](https://github.com/smerrill/vagrant-rsync-back)
- [vagrant-gatling-rsync](https://github.com/smerrill/vagrant-gatling-rsync) (optional)

**Guest machine to host machine**

```
$ vagrant rsync-back
```

**Host machine to guest machine**

```
$ vagrant rsync
```

## Configuration

**Installed Utils**

- curl
- unzip
- memcached

**Apache**

* SetEnv APPLICATION_ENV development

**PHP Modules**

- php-curl
- php-mcrypt
- php-gd
- php-mysql
- php-cli
- php-memcache
- php-intl
- php-xdebug
- php-xml
- php-simplexml
- php-zip

**PHP Settings**

param | value
--- | --- |
error_reporting | E_ALL
display_errors | On
upload_max_filesize | 10M
post_max_size | 10M
max_execution_time | 240
memory_limit | 512M
xdebug.remote_enable | 1
xdebug.scream | 1
xdebug.cli_color | 1
xdebug.show_local_vars | 1
xdebug.max_nesting_level | 400
timezone | Europe/London

**Mails**

Mails are catched and saved to `/var/log/mail/`

**Reconfigure**

You can change the variables (e.g. PrestaShop-Version) in env/*/group_vars/all and reconfigure it.

```
$ vagrant provision
```
